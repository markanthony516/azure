﻿configuration ConfigureMvaJump {

    Param (
    [Parameter( Mandatory )][String] $domainName,
    [Parameter( Mandatory )][System.Management.Automation.PSCredential] $adminCredentials,
    [Parameter( Mandatory )][String] $publicIpAddressFqdn
    )

    Import-DscResource -ModuleName xActiveDirectory, xNetworking, PSDesiredStateConfiguration
    [System.Management.Automation.PSCredential] $domainCredentials = New-Object System.Management.Automation.PSCredential ( "${domainName}\$($adminCredentials.UserName)", $adminCredentials.Password )
    $interface = Get-NetAdapter | Where-Object Name -Like "Ethernet*" | Select-Object -First 1
    $interfaceAlias = $( $interface.Name )

    Node localhost {

        Script AddADDSFeature {

            SetScript = { Add-WindowsFeature "AD-Domain-Services" -ErrorAction SilentlyContinue }
            GetScript = { @{ } }
            TestScript = { $False }

        }

        WindowsFeature DNS {

            Ensure = "Present"
            Name = "DNS"

        }

        Script EnableDnsDiagnostics {

            SetScript = {
                Write-Verbose -Verbose "Enabling DNS client diagnostics"
                Set-DnsServerDiagnostics -All $True
            }
            GetScript = { @{ } }
            TestScript = { $False }
            DependsOn = "[WindowsFeature]DNS"

        }

        WindowsFeature DnsTools {

            Ensure = "Present"
            Name = "RSAT-DNS-Server"

        }

        xDnsServerAddress DnsServerAddress {

            Address = '127.0.0.1'
            InterfaceAlias = $interfaceAlias
            AddressFamily = 'IPv4'
            DependsOn = "[WindowsFeature]DNS"

        }

        WindowsFeature ADDSInstall {

            Ensure = "Present"
            Name = "AD-Domain-Services"
            DependsOn="[Script]AddADDSFeature"

        }

        xADDomain FirstDS {

            DomainName = $domainName
            DomainAdministratorCredential = $domainCredentials
            SafemodeAdministratorPassword = $domainCredentials
            DatabasePath = "C:\NTDS"
            LogPath = "C:\NTDS"
            SysvolPath = "C:\SYSVOL"
            DependsOn = "[WindowsFeature]ADDSInstall"

        }

        LocalConfigurationManager {

            ConfigurationMode = 'ApplyOnly'
            RebootNodeIfNeeded = $True

        }

        WindowsFeature RDS-Gateway {

            Ensure = "Present"
            Name = "RDS-Gateway"

        }

        WindowsFeature RDS-Web-Access {

            Ensure = "Present"
            Name = "RDS-Web-Access"

        }

        WindowsFeature RSAT-AD-Tools {

            Ensure = "Present"
            Name = "RSAT-AD-Tools"
            IncludeAllSubFeature = $True

        }

        WindowsFeature RSAT-RDS-Tools {

            Ensure = "Present"
            Name = "RSAT-RDS-Tools"
            IncludeAllSubFeature = $True

        }

        Script ConfigureRdsGw {

            SetScript = {

                Import-Module RemoteDesktopServices
                Write-Verbose -Verbose "Creating RD CAP"
                Set-Location RDS:\GatewayServer\CAP
                New-Item -Name TestCAP -UserGroups "Domain Users@$($using:domainName)" -AuthMethod 1

                Write-Verbose -Verbose "Creating RD RAP"
                Set-Location RDS:\GatewayServer\RAP
                New-Item -Name TestRAP -UserGroups "Domain Users@$($using:domainName)" -ComputerGroupType 2

                Write-Verbose -Verbose "Creating RD certificate for fqdn: $($using:publicIpAddressFqdn)"
                New-SelfSignedCertificate -DnsName $using:publicIpAddressFqdn -CertStoreLocation cert:\LocalMachine\My

                Write-Verbose -Verbose "Configuring RDGW certificate"
                Get-ChildItem cert:\LocalMachine\My | Where-Object { $_.Subject -like "*$using:publicIpAddressFqdn" } | ForEach-Object { Set-Item -Path RDS:\GatewayServer\SSLCertificate\Thumbprint -Value $_.Thumbprint }

                Write-Verbose -Verbose "Restarting TSGateway"
                Restart-Service TSGateway

            }

            GetScript = { @{ } }
            TestScript = { $False }
            DependsOn = "[WindowsFeature]RSAT-RDS-Tools"

        }

    }

}
